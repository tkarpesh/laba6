Program Laba6;

{$APPTYPE CONSOLE}

uses
  SysUtils;

var
  //Matrix:
  Matrix: array of array of Integer;

  //The size of Matrix:
  N, M: Integer;

  //Counters:
  I, J, K, P: Integer;

  //Additional variables:
  Dop, U, D: Integer;

begin
  Writeln('Enter N, M - ');
  Readln(N, M);
  SetLength(Matrix, N, M);
  Randomize;
  //Filling the Matrix:
  For I := 0 To (N - 1) do
  begin
    for J := 0 to (M - 1) do
    begin
      Matrix[I, J] := Random(10);
      Write(Matrix[I, J]:3);
    end;
    Writeln(#13#10);
  end;
  //Columns' sort:
  for J := 0 to M - 1 do
  begin
    U := 1;
    D := N - 1;
    K := N - 1;
    for I := 0 To N - 1 do
    begin
      Repeat
        for P := D downto U do
        begin
          If Matrix[P - 1, J] <= Matrix[P, J] then
          begin
            Dop := Matrix[P - 1, J];
            Matrix[P - 1, J] := Matrix[P, J];
            Matrix[P, J] := Dop;
          end;
          K := P;
        end;
        U := K + 1;
        for P := U to D do
        begin
          If Matrix[P - 1, J] <= Matrix[P, J] then
          begin
            Dop := Matrix[P - 1, J];
            Matrix[P - 1, J] := Matrix[P, J];
            Matrix[P, J] := Dop;
          end;
          K := P;
        end;
        D := K - 1;
      until U > D;
    end;
  end;
  //Strings' sort:
  For I := 0 To (N - 1) do
  begin
    For J := 1 To (M - 1) do
    begin
      Dop := Matrix[I, J];
      K := J - 1;
      While (Matrix[I, K] >= Dop) and (K >= 0) do
        begin
           Matrix[I, K + 1] := Matrix[I, K];
           K := K - 1;
        end;
        Matrix[I, K + 1] := Dop;
    end;
  end;
  Writeln('Result:', #13#10);
  For I := 0 To (N - 1) do
  begin
    for J := 0 to (M- 1) do
    begin
      Write(Matrix[I, J]:3);
    end;
    Writeln(#13#10);
  end;
  Readln;
end.
